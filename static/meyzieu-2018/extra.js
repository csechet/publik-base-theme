$(function() {
    $('div.carrousel-wrapper .menu a').click(function(e) {
        e.preventDefault();
        var $selected = $(this).parents('li');
        $.each($selected.parents('ul').children(), function(index, element) {
            $(element).removeClass('selected');
        });
        $(this).parents('li').addClass('selected');
        var page_url = this.href;
        var content_selector = 'div.carrousel-wrapper .content';
        $(content_selector).fadeTo(100, 0, function() {
            $(content_selector).load(page_url + ' #main-content', function() {
                $(content_selector).fadeTo(100, 1);
            });
        });
    });
    $('div.carrousel-wrapper .menu a:first').click();
});
