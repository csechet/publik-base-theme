function handle_details_display() {
  $('div.contact-commune select, div.contact-commune-rgpd select, div.coordonnees-generales select').on('change', function() {
    var $parent = $(this).parent();
    $parent.find('> div').hide();
    $parent.find('div#commune-' + this.value).show();
  });
}

$(function() {
  handle_details_display();
  $(document).on('combo:cell-loaded load', function() {
    handle_details_display();
    $('div.contact-commune select, div.coordonnees-generales select').trigger('change');
  });
});
