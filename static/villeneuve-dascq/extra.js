// TOP Search CELL
// Fix cell On right of window when she's activated (input get focus)
(function(d){
	'use strict';
	let top_search;
	let el_to_move;
	let closeBtn;
	let mask;

	// CSS class name
	const fixed = "search-cell-fixed";
	const on_top = "search-cell-move-on-top";

	// Status
	let is_fixed = false;
	let is_fixed_on_top = false;

	const debug = function(func) {
		console.log(func);
		console.log("is_fixed", is_fixed);
		console.log("is_fixed_on_top", is_fixed_on_top);
	};

	const createCloseBtn = function(){
		closeBtn = d.createElement('button');
		closeBtn.className = "top-search-close-btn";
		closeBtn.setAttribute("aria-label", "Fermer la fenêtre de recherche");
		closeBtn.setAttribute("role", "button");
	};

	const addCloseBtn = function(){
		createCloseBtn();
		$(el_to_move).prepend(closeBtn);
	};

	const createMask = function() {
		mask = d.createElement('span');
		mask.className = "top-search-mask";
		mask.setAttribute("role", "button");

		top_search.append(mask);
	};

	const fixLayout = function() {
		if (is_fixed) return;

		const elLayout = el_to_move.getBoundingClientRect()

		el_to_move.style.top = elLayout.top + "px";
		el_to_move.style.left = elLayout.left + "px";
		el_to_move.style.height = elLayout.height + "px";
		el_to_move.style.width = elLayout.width + "px";

		top_search.style.height = elLayout.height + "px";

		top_search.classList.add(fixed);
		is_fixed = true;
	};

	const removeLayout = function() {
		if (!is_fixed) return;
		top_search.classList.remove(fixed);
		top_search.removeAttribute('style');
		el_to_move.removeAttribute('style');
		is_fixed = false;
	};

	const fixOnTop = function(callback) {
		if ( !is_fixed || top_search.classList.contains(on_top) ) return;
		top_search.classList.add(on_top);
		is_fixed_on_top = true;
	};

	const detachTop = function(callback) {
		if ( !is_fixed_on_top || !top_search.classList.contains(on_top) ) return;
		$(el_to_move).one('transitionend', function(e){
			e.stopPropagation();
			if (callback) {
				callback();
			}
		});
		top_search.classList.remove(on_top);
		is_fixed_on_top = false;
	};

	const open = function() {
		if (is_fixed_on_top) return;
		d.body.classList.add('no-overflow');
		fixLayout();
		setTimeout(function () {
			fixOnTop();
		}, 20);
	};

	const close = function() {
		if (!is_fixed_on_top) return;
		detachTop(function(){
			removeLayout();
			d.body.classList.remove('no-overflow');
		})
	};

	const init = function() {
		top_search = d.querySelector('.pwa-navigation--wrapper .search-cell');
		if (!top_search) return;

		el_to_move = top_search.querySelector('div');
		const form = top_search.querySelector('form');
		const input = top_search.querySelector('input');

		addCloseBtn();
		createMask();

		// when focus change
		document.addEventListener('focusin', function(e) {
			if (e.target === input && !is_fixed_on_top) {
					open();
			} else {
				// close if activeElement is not a child of el_to_move
				if ( !el_to_move.contains(d.activeElement) ) {
					close();
				}
			}
		});

		// Open Input click
		$(input).on('click keydown', function(){
			if (input === d.activeElement) {
				open();
			}
		});

		// open when submit form
		$(form).on('submit', open);

		// close esc
		document.addEventListener('keydown', function(e){
			if (e.keyCode === 27) {
				close();
			}
		});

		// close click btn
		$(closeBtn).on('click', close);

		// close on mask
		$(mask).on('click', close);
	};

	$(function() {
		init();
	});
})(document)