$(function() {
  // from django/contrib/admin/static/admin/js/urlify.js
  var LATIN_MAP = {
    'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE',
    'Ç': 'C', 'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I',
    'Î': 'I', 'Ï': 'I', 'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O',
    'Õ': 'O', 'Ö': 'O', 'Ő': 'O', 'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U',
    'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH', 'Ÿ': 'Y', 'ß': 'ss', 'à': 'a',
    'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
    'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i',
    'ï': 'i', 'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o',
    'ö': 'o', 'ő': 'o', 'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u',
    'ű': 'u', 'ý': 'y', 'þ': 'th', 'ÿ': 'y'
  };

  function downcode(string) {
    return string.toLowerCase().replace(/[^A-Za-z0-9\[\] ]/g,function(a){ return LATIN_MAP[a]||a })
  };

  $('#actu img').on('click', function() {
    set_newsitems();
    $('#main-content').toggleClass('news-open');
    $(this).parent().toggleClass('open');
  });
  function set_newsitems() {
    var $feed_content = $('footer .feed-content');
    if ($feed_content.length == 0) return;
    $('#actu-content .feed-content').replaceWith($feed_content);
    $('#actu-content a.rss').attr('href', $('[data-feed-url]').data('feed-url'));
    $feed_content.show();
  }
  $('#nav-tracking-code form').on('submit', function() {
    var $real_form = $('.wcs-tracking-code-input form');
    $real_form.find('input#tracking-code').val($(this).find('input').val());
    $real_form.submit();
    return false;
  });
  $('.gru-content').delegate('div.wcsformsofcategorycell h2', 'click', function() {
    $(this).parents('div').first().toggleClass('opened');
  });
  $('#section-particuliers #search button').on('click', function() {
    $('#section-particuliers #search input').trigger('paste');
    return false;
  });
  $('#section-particuliers #search input').on('change keyup paste', function() {
    var q = $(this).val();
    var $search_results = $('#section-particuliers .search-results');
    if (!q) {
      $search_results.empty().hide();
      $('#section-particuliers .wcsformsofcategorycell').show();
    } else {
      $('#section-particuliers .wcsformsofcategorycell').hide();
      q = downcode(q);
      var results = Array();
      $('#section-particuliers .wcsformsofcategorycell li').each(function(idx, elem) {
        var haystack = downcode($(elem).data('keywords') + ' ' + $(elem).text());
        if (haystack.search(q) != -1) {
          results.push(elem);
        }
      });
      if (results.length == 0) {
        $search_results.empty();
        $('<p class="no-hits">Aucun résultat.</p>').appendTo($search_results);
      } else {
        $search_list = $('<ul>');
        $(results).each(function(idx, hit) {
          $(hit).clone().appendTo($search_list);
        });
        $search_results.empty();
        $search_list.appendTo($search_results);
      }
      $search_results.show();
    }
    return false;
  });
  $('#toplinks').on('click', function(e) {
    var $a = $('#toplinks a').first();
    if ($a.length == 1) {
      window.location = $a.attr('href');
    }
    ev.stopPropagation();
    return false;
  });
});
