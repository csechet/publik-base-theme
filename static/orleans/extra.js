$(function() {
  $('div.wcsformsofcategorycell').delegate('h2', 'click', function() {
    $(this).parents('div.wcsformsofcategorycell').toggleClass('toggled');
  });
  /* reset error class when contents is changed */
  var $widgets_with_error = $('.widget-with-error');
  if ($widgets_with_error.length) {
    $widgets_with_error
      .find('.content').addClass('with-padding')
      .on('change', 'input, select, textarea', function() {
        $(this).parents('.widget-with-error').removeClass('widget-with-error');
      })
  }
});
