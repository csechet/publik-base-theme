$(function() {
  /* add a class to body when the page got scrolled */
  $(window).on('scroll', function() {
    if ($(window).scrollTop() == 0) {
      $('body').removeClass('scrolled');
    } else {
      $('body').addClass('scrolled');
    }
  });
  /* reset error class when contents is changed */
  $('input, select, textarea').on('change', function() {
    $(this).parents('.widget-with-error').removeClass('widget-with-error');
  });
});
