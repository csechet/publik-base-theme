$(function () {
  $('a.show-scopes').on('click', function(event) {
    $(this).hide();
    $('a.hide-scopes').show();
    $('#scopes').show();
  });
  $('a.hide-scopes').on('click', function(event) {
    $(this).hide();
    $('a.show-scopes').show();
    $('#scopes').hide();
  });
});
