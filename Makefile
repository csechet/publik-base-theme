VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="publik-base-theme"
INKSCAPE=$(shell pwd)/src/inkscape_wrapper.py

prefix = /usr

all: themes.json icons css

static/includes/_data_uris.scss: $(wildcard static/includes/img/*)
	python3 make_data_uris.py static/includes/

static/toodego/_data_uris.scss: $(wildcard static/toodego/img/*)
	python3 make_data_uris.py static/toodego/

static/lille/_data_uris.scss: $(wildcard static/lille/img/*)
	python3 make_data_uris.py static/lille/

static/lomme/_data_uris.scss: $(wildcard static/lomme/img/*)
	python3 make_data_uris.py static/lomme/

static/hellemmes/_data_uris.scss: $(wildcard static/hellemmes/img/*)
	python3 make_data_uris.py static/hellemmes/

static/toulouse-2022/_data_uris.scss: $(wildcard static/toulouse-2022/img/*)
	python3 make_data_uris.py static/toulouse-2022/

themes.json: $(wildcard static/*/config.json) help/fr/misc-scss.page
	python3 create_themes_json.py

%.css: export LC_ALL=C.UTF-8
.SECONDEXPANSION:
%.css: %.scss $(wildcard static/includes/*.scss  static/includes/*/*.scss) static/includes/_data_uris.scss static/lille/_data_uris.scss static/lomme/_data_uris.scss static/hellemmes/_data_uris.scss static/toodego/_data_uris.scss static/toulouse-2022/_data_uris.scss $$(wildcard $$(@D)/*.scss)
	sassc --sourcemap $< $@

css: $(shell python3 get_themes.py) static/portal-agent/css/agent-portal.css static/includes/gadjo-extra.css
	rm -rf static/*/.sass-cache/

icons:
	# chateauroux
	cd src/ && python3 render-imgs-dashboard.py ../static/chateauroux/img/ --normal 333333 --selected 0779B7 --title FFFFFF --title-width 80
	# orleans
	cd src/ && python3 render-imgs-categories.py ../static/orleans/img/ --primary f05923 --secondary 34697D
	cd src/ && python3 render-imgs-dashboard.py ../static/orleans/img/ --normal FFFFFF --normal-width 30 --selected f05923 --selected-width 30 --title FFFFFF --title-width 80
	# publik
	cd src/ && python3 render-imgs-categories.py ../static/publik/img/
	cd src/ && python3 render-imgs-dashboard.py ../static/publik/img/ --normal 4D4D4D --selected DF017A --title FFFFFF --title-width 80
	# somme
	cd src/ && python3 render-imgs-categories.py ../static/somme-cd80/img/ --primary A8002B --secondary A8002B
	cd src/ && python3 render-imgs-dashboard.py ../static/somme-cd80/img/ --normal 4D4D4D --selected 87A738 --title FFFFFF --title-width 80
	# tournai
	cd static/tournai/ && for F in assets/*.svg; do $(INKSCAPE) --without-gui --file $$F --export-area-drawing --export-area-snap --export-png img/$$(basename $$F .svg).png --export-width 40; done

clean:
	rm -rf sdist
	rm -f src/tmp-*.svg
	rm -f static/*/_data_uris.scss

DIST_FILES = \
	Makefile \
	desc.xml \
	create_themes_json.py \
	get_themes.py \
	make_data_uris.py \
	static templates \
	src \
	help

dist: clean
	-mkdir sdist
	rm -rf sdist/$(NAME)-$(VERSION)
	mkdir -p sdist/$(NAME)-$(VERSION)
	for i in $(DIST_FILES); do \
		cp -R "$$i" sdist/$(NAME)-$(VERSION); \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/publik/themes/publik-base
	cp -r static templates themes.json desc.xml $(DESTDIR)$(prefix)/share/publik/themes/publik-base
	rm $(DESTDIR)$(prefix)/share/publik/themes/publik-base/static/*/config.json
	mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/
	ln -s $(prefix)/share/publik/themes/publik-base $(DESTDIR)$(prefix)/share/wcs/themes/publik-base

dist-bzip2: dist
	-mkdir sdist
	cd sdist && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 $(NAME)-$(VERSION)

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))
